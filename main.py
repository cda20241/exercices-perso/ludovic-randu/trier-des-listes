import random

liste = [['pascal', 'rose', 43],
         ['mickaël', 'fleur', 29],
         ['henry', 'tulipe', 35],
         ['michel', 'framboise', 35],
         ['arthur', 'petale', 35],
         ['michel', 'pollen', 50],
         ['maurice', 'framboise', 42]]

random.shuffle(liste)


def trier(personnes, index, boul):
    """
    :param personnes: envoi la liste des personnes à trier
    :param index: choix de tri (0 pour les prénoms, 1 pour les noms, 2 pour l'âge
    :param boul: pour inverser le sens de la liste
    :return: renvoi la liste ligne par ligne
    """
    for i in range(len(personnes)):
        mini = i
        for j in range(i+1, len(personnes)):
            if personnes[j][index] < personnes[mini][index]:
                mini = j
        personnes[i], personnes[mini] = personnes[mini], personnes[i]
    if boul:
        personnes = personnes[::-1]
    for i in personnes:
        print(i[0] + ' ' + i[1] + ' ' + str(i[2]) + ' ans')
    print('')


def trier_prenom():
    """
    Fonction qui tri d'abord par nom puis si il y a des doublons de noms tri par prénoms
    :return:
    """
    for i in range(len(liste)):
        mini = i
        for j in range(i+1, len(liste)):
            if liste[j][1] < liste[mini][1]:
                mini = j
        liste[i], liste[mini] = liste[mini], liste[i]
        for a in range(len(liste)):
            mini = a
            for j in range(a + 1, len(liste)):
                if liste[j][1] == liste[mini][1]:
                    if liste[j][0][0] < liste[mini][0][0]:
                        mini = j
                        liste[a], liste[mini] = liste[mini], liste[a]
                    elif liste[j][0][1] < liste[mini][0][1]:
                        mini = j
                        liste[a], liste[mini] = liste[mini], liste[a]
    for i in liste:
        print(i[0] + ' ' + i[1] + ' ' + str(i[2]) + ' ans')
    print('')


# 1 Liste 1 Par ordre alphabetique des noms
print("Liste 1: Tri par ordre alphabétique des noms")
trier(liste, 1, False)


# 2 Liste 2 Par ordre alphabetique des prénoms
print("Liste 2: Tri par ordre alphabétique inversé des prénoms")
trier(liste, 0, True)


# 3 Liste 3 Par age
print("Liste 3: Tri par ordre d'âge")
trier(liste, 2, False)


# 4 Liste 4 par orde alphabetique des noms puis prénom
print("Liste 4: tri par nom puis par prénom si nom identique")
trier_prenom()
